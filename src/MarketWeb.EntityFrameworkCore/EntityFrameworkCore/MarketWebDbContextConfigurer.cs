using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace MarketWeb.EntityFrameworkCore
{
    public static class MarketWebDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<MarketWebDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<MarketWebDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
