﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using MarketWeb.Configuration;
using MarketWeb.Web;

namespace MarketWeb.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class MarketWebDbContextFactory : IDesignTimeDbContextFactory<MarketWebDbContext>
    {
        public MarketWebDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MarketWebDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            MarketWebDbContextConfigurer.Configure(builder, configuration.GetConnectionString(MarketWebConsts.ConnectionStringName));

            return new MarketWebDbContext(builder.Options);
        }
    }
}
