﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using MarketWeb.Authorization.Roles;
using MarketWeb.Authorization.Users;
using MarketWeb.MultiTenancy;
using MarketWeb.Companies;
using MarketWeb.Products;
using MarketWeb.Orders;
using MarketWeb.Carts;

namespace MarketWeb.EntityFrameworkCore
{
    public class MarketWebDbContext : AbpZeroDbContext<Tenant, Role, User, MarketWebDbContext>
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<CartItem> CartItems { get; set; }

        public MarketWebDbContext(DbContextOptions<MarketWebDbContext> options)
            : base(options)
        {
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);
        //    modelBuilder.Entity<CartItem>(b =>
        //    {
        //        b.HasKey(ci => new { ci.CartID, ci.ProductID });
        //        b.HasOne(ci => ci.Cart).WithMany(cart => cart.Items).HasForeignKey(ci => ci.CartID);
        //        b.HasOne(ci => ci.Product).WithMany().HasForeignKey(ci => ci.ProductID).OnDelete(DeleteBehavior.Restrict);
        //    });
        //    modelBuilder.Entity<OrderItem>(b =>
        //    {
        //        b.HasKey(oi => new { oi.OrderID, oi.ProductID });
        //        b.HasOne(oi => oi.Order).WithMany(order => order.Items).HasForeignKey(oi => oi.OrderID);
        //        b.HasOne(oi => oi.Product).WithMany().HasForeignKey(oi => oi.ProductID).OnDelete(DeleteBehavior.Restrict);
        //    });
        //}
    }
}
