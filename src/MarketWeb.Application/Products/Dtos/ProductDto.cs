﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MarketWeb.Companies.Dtos;
using System.ComponentModel.DataAnnotations;

namespace MarketWeb.Products.Dtos
{
    [AutoMap(typeof(Product))]
    public class ProductDto : EntityDto<long>
    {
        [Required, StringLength(Product.ProductNameMaxLength)]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public long CompanyID { get; set; }
        public CompanyDto Company { get; set; }
    }
}
