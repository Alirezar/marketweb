﻿using Abp.Application.Services.Dto;

namespace MarketWeb.Products.Dtos
{
    public class PagedProductResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}
