﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using MarketWeb.Authorization;
using MarketWeb.Products.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketWeb.Products
{
    public class ProductAppService : AsyncCrudAppService<Product, ProductDto, long, PagedProductResultRequestDto>, IProductAppService
    {
        public ProductAppService(IRepository<Product, long> repository)
            : base(repository)
        {
        }

        protected override string CreatePermissionName { get => PermissionNames.ProductsARU; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => PermissionNames.ProductsARU; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => PermissionNames.ProductsARU; set => base.DeletePermissionName = value; }

        public override async Task<PagedResultDto<ProductDto>> GetAllAsync(PagedProductResultRequestDto input)
        {
            List<Product> products;
            if (string.IsNullOrEmpty(input.Keyword))
                products = await Repository.GetAllIncluding(p => p.Company).ToListAsync();
            else
                products = await Repository.GetAllIncluding(p => p.Company).Where(p => p.Name.Contains(input.Keyword)).ToListAsync();

            return new PagedResultDto<ProductDto>(products.Count, ObjectMapper.Map<List<ProductDto>>(products));
        }

        public override async Task<ProductDto> GetAsync(EntityDto<long> input)
        {
            var product = await Repository.GetAllIncluding(p => p.Company).FirstOrDefaultAsync(p => p.Id == input.Id);
            return MapToEntityDto(product);
        }

        protected override IQueryable<Product> CreateFilteredQuery(PagedProductResultRequestDto input)
        {
            return (string.IsNullOrEmpty(input.Keyword)) ? base.CreateFilteredQuery(input) : base.CreateFilteredQuery(input).Where(p => p.Name.Contains(input.Keyword));
        }
    }
}
