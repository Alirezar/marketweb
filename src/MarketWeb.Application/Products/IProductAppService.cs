﻿using Abp.Application.Services;
using MarketWeb.Products.Dtos;

namespace MarketWeb.Products
{
    public interface IProductAppService : IAsyncCrudAppService<ProductDto, long, PagedProductResultRequestDto, ProductDto>
    {
    }
}
