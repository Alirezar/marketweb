using System.ComponentModel.DataAnnotations;

namespace MarketWeb.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}