﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MarketWeb.Authorization;

namespace MarketWeb
{
    [DependsOn(
        typeof(MarketWebCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MarketWebApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<MarketWebAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(MarketWebApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
