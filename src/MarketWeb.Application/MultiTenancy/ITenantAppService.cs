﻿using Abp.Application.Services;
using MarketWeb.MultiTenancy.Dto;

namespace MarketWeb.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

