﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MarketWeb.Sessions.Dto;

namespace MarketWeb.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
