﻿using MarketWeb.Authorization.Users;
using System.Collections.Generic;

namespace MarketWeb.Carts.Dtos
{
    public class CartDto
    {
        public long UserID { get; set; }
        public User User { get; set; }

        public IList<CartItemDto> Items { get; set; }

        public decimal TotalCartPrice()
        {
            decimal result = 0M;
            foreach (var item in Items)
                result += item.TotalPrice();
            return result;
        }
    }
}
