﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MarketWeb.Products.Dtos;
using MarketWeb.Users.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace MarketWeb.Carts.Dtos
{
    [AutoMap(typeof(CartItem))]
    public class CartItemDto : EntityDto<long>
    {
        public long UserID { get; set; }
        public UserDto User { get; set; }

        public long ProductID { get; set; }
        public ProductDto Product { get; set; }

        [Required, Range(1, int.MaxValue)]
        public int Count { get; set; }

        public decimal TotalPrice() => Product.Price * Count;
    }
}
