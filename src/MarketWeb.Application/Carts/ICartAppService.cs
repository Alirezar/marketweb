﻿using Abp.Application.Services;
using MarketWeb.Carts.Dtos;
using MarketWeb.Products.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarketWeb.Carts
{
    public interface ICartAppService : IAsyncCrudAppService<CartItemDto, long>
    {
        Task<CartItemDto> GetAsync(long userId, long productId);
        Task<CartItemDto> GetUserCartItemAsync(long productId);
        Task<CartDto> GetUserCartAsync();
        Task<ICollection<ProductDto>> GetUserCartProductsAsync();
        Task CleanUserCart();
        Task RemoveItemFromUserCart(long productId);
        Task UpdateItemCount(long productId, int count);
    }
}
