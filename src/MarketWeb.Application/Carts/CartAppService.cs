﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using MarketWeb.Carts.Dtos;
using MarketWeb.Products.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketWeb.Carts
{
    [AbpAuthorize]
    public class CartAppService : AsyncCrudAppService<CartItem, CartItemDto, long>, ICartAppService
    {
        public CartAppService(IRepository<CartItem, long> repository) 
            : base(repository)
        {
        }

        public async Task CleanUserCart()
        {
            var cart = await GetUserCartAsync();
            foreach (var item in cart.Items)
                await DeleteAsync(new EntityDto<long> { Id = item.Id });
        }

        public override async Task<PagedResultDto<CartItemDto>> GetAllAsync(PagedAndSortedResultRequestDto input)
        {
            var carts = await GetAllIncludeProductAndUser().ToListAsync();
            return new PagedResultDto<CartItemDto>(carts.Count, ObjectMapper.Map<List<CartItemDto>>(carts));
        }

        public override async Task<CartItemDto> GetAsync(EntityDto<long> input)
        {
            return MapToEntityDto(await GetAllIncludeProductAndUser().FirstOrDefaultAsync(ci => ci.Id == input.Id));
        }

        public async Task<CartItemDto> GetUserCartItemAsync(long productId)
        {
            var item = await GetAllIncludeProductAndUser().FirstOrDefaultAsync(ci => ci.ProductID == productId && ci.UserID == AbpSession.UserId.Value);
            return MapToEntityDto(item);
        }

        public async Task<CartDto> GetUserCartAsync()
        {
            var userCartItems = await GetAllIncludeProductAndUser().Where(ci => ci.UserID == AbpSession.UserId.Value).ToListAsync();
            return new CartDto { UserID = AbpSession.UserId.Value, Items = ObjectMapper.Map<List<CartItemDto>>(userCartItems) };
        }

        public async Task RemoveItemFromUserCart(long productId)
        {
            var item = await GetUserCartItemAsync(productId);
            if (item != null)
                await DeleteAsync(item);
        }

        public async Task UpdateItemCount(long productId, int count)
        {
            var item = await GetUserCartItemAsync(productId);
            if (item != null)
            {
                item.Count = count;
                await UpdateAsync(item);
            }
        }

        private IQueryable<CartItem> GetAllIncludeProductAndUser()
        {
            return Repository.GetAllIncluding(ci => ci.User, ci => ci.Product);
        }

        public async Task<CartItemDto> GetAsync(long userId, long productId)
        {
            return MapToEntityDto(await GetAllIncludeProductAndUser().FirstOrDefaultAsync(ci => ci.UserID == userId && ci.ProductID == productId));
        }

        public async Task<ICollection<ProductDto>> GetUserCartProductsAsync()
        {
            var cartProducts = (await GetUserCartAsync()).Items.Select(item => item.Product).ToList();
            return cartProducts;
        }
    }
}
