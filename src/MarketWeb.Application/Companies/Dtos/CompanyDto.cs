﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MarketWeb.Products.Dtos;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketWeb.Companies.Dtos
{
    [AutoMap(typeof(Company))]
    public class CompanyDto : EntityDto<long>
    {
        [Required, StringLength(Company.MaxCompanyNameLength)]
        public string Name { get; set; }

        [StringLength(Company.MaxCompanyDiscriptionLength)]
        public string Discription { get; set; }

        public ICollection<ProductDto> Products { get; set; }
    }
}
