﻿using Abp.Application.Services.Dto;

namespace MarketWeb.Companies.Dtos
{
    public class PagedCompanyResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}
