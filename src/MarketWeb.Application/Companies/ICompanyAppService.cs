﻿using Abp.Application.Services;
using MarketWeb.Companies.Dtos;

namespace MarketWeb.Companies
{
    public interface ICompanyAppService : IAsyncCrudAppService<CompanyDto, long, PagedCompanyResultRequestDto>
    {
    }
}
