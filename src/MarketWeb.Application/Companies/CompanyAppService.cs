﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using MarketWeb.Authorization;
using MarketWeb.Companies.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarketWeb.Companies
{
    public class CompanyAppService : AsyncCrudAppService<Company, CompanyDto, long, PagedCompanyResultRequestDto>, ICompanyAppService
    {
        public CompanyAppService(IRepository<Company, long> repository) 
            : base(repository)
        {
        }

        protected override string CreatePermissionName { get => PermissionNames.CompaniesARU; set => base.CreatePermissionName = value; }
        protected override string UpdatePermissionName { get => PermissionNames.CompaniesARU; set => base.UpdatePermissionName = value; }
        protected override string DeletePermissionName { get => PermissionNames.CompaniesARU; set => base.DeletePermissionName = value; }

        public override async Task<PagedResultDto<CompanyDto>> GetAllAsync(PagedCompanyResultRequestDto input)
        {
            var companies = await Repository.GetAllIncluding(c => c.Products).ToListAsync();
            return new PagedResultDto<CompanyDto>(companies.Count, ObjectMapper.Map<List<CompanyDto>>(companies));
        }

        public override async Task<CompanyDto> GetAsync(EntityDto<long> input)
        {
            var company = await Repository.GetAllIncluding(c => c.Products).FirstOrDefaultAsync(c => c.Id == input.Id);
            return MapToEntityDto(company);
        }
    }
}
