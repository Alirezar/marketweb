﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using MarketWeb.Configuration.Dto;

namespace MarketWeb.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : MarketWebAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
