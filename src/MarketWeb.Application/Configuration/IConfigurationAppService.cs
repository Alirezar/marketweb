﻿using System.Threading.Tasks;
using MarketWeb.Configuration.Dto;

namespace MarketWeb.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
