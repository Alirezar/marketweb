﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using MarketWeb.Products.Dtos;

namespace MarketWeb.Orders.Dtos
{
    [AutoMap(typeof(OrderItem))]
    public class OrderItemDto : EntityDto<long>
    {
        public long OrderID { get; set; }
        public OrderDto Order { get; set; }

        public long ProductID { get; set; }
        public ProductDto Product { get; set; }

        public int Count { get; set; }

        public decimal TotalPrice() => Product.Price * Count;
    }
}
