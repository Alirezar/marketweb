﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using MarketWeb.Users.Dto;
using System;
using System.Collections.Generic;

namespace MarketWeb.Orders.Dtos
{
    [AutoMap(typeof(Order))]
    public class OrderDto : EntityDto<long>, IHasCreationTime
    {
        public long UserID { get; set; }
        public UserDto User { get; set; }

        public OrderState.States State { get; set; }

        public DateTime CreationTime { get; set; }

        public ICollection<OrderItemDto> Items { get; set; }

        public decimal TotalOrderPrice()
        {
            decimal result = 0;
            foreach (var item in Items)
                result += item.TotalPrice();
            return result;
        }
    }
}
