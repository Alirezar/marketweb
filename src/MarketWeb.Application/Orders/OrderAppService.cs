﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using MarketWeb.Carts.Dtos;
using MarketWeb.Orders.Dtos;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketWeb.Orders
{
    [AbpAuthorize]
    public class OrderAppService : AsyncCrudAppService<Order, OrderDto, long>, IOrderAppService
    {
        private readonly IRepository<OrderItem, long> _orderItemRepository;
        public OrderAppService(IRepository<Order, long> repository, IRepository<OrderItem, long> orderItemRepository)
            : base(repository)
        {
            _orderItemRepository = orderItemRepository;
        }

        public async Task<OrderDto> CreateAsync(IList<CartItemDto> cartItems)
        {
            long userId = AbpSession.UserId.Value;
            var order = await CreateAsync(new OrderDto { UserID = userId, State = OrderState.States.Boxing });
            foreach (var item in cartItems)
                await _orderItemRepository.InsertAsync(new OrderItem { OrderID = order.Id, ProductID = item.ProductID, Count = item.Count });
            return await GetAsync(new EntityDto<long> { Id = order.Id });
        }

        public override async Task<PagedResultDto<OrderDto>> GetAllAsync(PagedAndSortedResultRequestDto input)
        {
            var orders = await GetAllIncludeItemsAndUser().ToListAsync();
            return new PagedResultDto<OrderDto>(orders.Count, ObjectMapper.Map<List<OrderDto>>(orders));
        }

        public override async Task<OrderDto> GetAsync(EntityDto<long> input)
        {
            return MapToEntityDto(await GetAllIncludeItemsAndUser().FirstOrDefaultAsync(o => o.Id == input.Id));
        }

        public async Task<ListResultDto<OrderDto>> GetUserOrders()
        {
            var userOrders = await GetAllIncludeItemsAndUser().Where(o => o.UserID == AbpSession.UserId.Value).ToListAsync();
            return new ListResultDto<OrderDto>(ObjectMapper.Map<List<OrderDto>>(userOrders));
        }

        private IQueryable<Order> GetAllIncludeItemsAndUser()
        {
            return Repository.GetAllIncluding(o => o.User, o => o.Items).OrderByDescending(o => o.CreationTime);
        }
    }
}
