﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MarketWeb.Carts.Dtos;
using MarketWeb.Orders.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarketWeb.Orders
{
    public interface IOrderAppService : IAsyncCrudAppService<OrderDto, long>
    {
        Task<ListResultDto<OrderDto>> GetUserOrders();
        Task<OrderDto> CreateAsync(IList<CartItemDto> cartItems);
    }
}
