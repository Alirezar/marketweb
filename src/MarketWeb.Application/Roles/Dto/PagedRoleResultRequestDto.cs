﻿using Abp.Application.Services.Dto;

namespace MarketWeb.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

