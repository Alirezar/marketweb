﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MarketWeb.Configuration;

namespace MarketWeb.Web.Host.Startup
{
    [DependsOn(
       typeof(MarketWebWebCoreModule))]
    public class MarketWebWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public MarketWebWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MarketWebWebHostModule).GetAssembly());
        }
    }
}
