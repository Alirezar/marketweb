﻿using Abp.Domain.Entities;
using MarketWeb.Products;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarketWeb.Orders
{
    public class OrderItem : Entity<long>
    {
        [Required, ForeignKey("Order")]
        public long OrderID { get; set; }
        public Order Order { get; set; }

        [Required, ForeignKey("Product")]
        public long ProductID { get; set; }
        public Product Product { get; set; }

        [Required, Range(1, int.MaxValue)]
        public int Count { get; set; }

        public override bool IsTransient() => false;
    }
}
