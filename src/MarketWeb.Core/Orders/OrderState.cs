﻿using System.Collections.Generic;

namespace MarketWeb.Orders
{
    public class OrderState
    {
        public enum States : byte
        {
            Boxing,
            Sended,
            Recived
        }
        public static Dictionary<States, string> State2String { get; } = new Dictionary<States, string>
        {
            [States.Boxing] = "Boxing",
            [States.Sended] = "Sended",
            [States.Recived] = "Recived"
        };
    }
}
