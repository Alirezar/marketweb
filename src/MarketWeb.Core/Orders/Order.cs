﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MarketWeb.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarketWeb.Orders
{
    public class Order : Entity<long>, IHasCreationTime
    {
        [Required, ForeignKey("Customer")]
        public long UserID { get; set; }
        public User User { get; set; }

        [Required]
        public OrderState.States State { get; set; }

        public DateTime CreationTime { get; set; }

        public ICollection<OrderItem> Items { get; set; }

        public override bool IsTransient() => false;
    }
}
