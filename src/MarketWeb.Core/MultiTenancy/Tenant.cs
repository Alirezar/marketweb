﻿using Abp.MultiTenancy;
using MarketWeb.Authorization.Users;

namespace MarketWeb.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
