﻿using Abp.Domain.Entities;
using MarketWeb.Companies;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarketWeb.Products
{
    public class Product : Entity<long>
    {
        public const int ProductNameMaxLength = 60;

        [Required, StringLength(ProductNameMaxLength)]
        public string Name { get; set; }

        [Required, Column(TypeName = "decimal(12,2)")]
        public decimal Price { get; set; }

        [Required, ForeignKey("Company")]
        public long CompanyID { get; set; }
        public Company Company { get; set; }
    }
}
