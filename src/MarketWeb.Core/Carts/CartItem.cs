﻿using Abp.Domain.Entities;
using MarketWeb.Authorization.Users;
using MarketWeb.Products;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MarketWeb.Carts
{
    public class CartItem : Entity<long>
    {
        [Required, ForeignKey("User")]
        public long UserID { get; set; }
        public User User { get; set; }

        [Required, ForeignKey("Product")]
        public long ProductID { get; set; }
        public Product Product { get; set; }

        [Required, Range(1, int.MaxValue)]
        public int Count { get; set; }

        public override bool IsTransient() => true;
    }
}
