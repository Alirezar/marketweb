﻿using Abp.Authorization;
using MarketWeb.Authorization.Roles;
using MarketWeb.Authorization.Users;

namespace MarketWeb.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
