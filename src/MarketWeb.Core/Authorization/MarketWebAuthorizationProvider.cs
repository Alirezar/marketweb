﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace MarketWeb.Authorization
{
    public class MarketWebAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Orders, L("Orders"));
            context.CreatePermission(PermissionNames.CompaniesARU, L("CompaniesManagment"));
            context.CreatePermission(PermissionNames.ProductsARU, L("ProductsManagment"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, MarketWebConsts.LocalizationSourceName);
        }
    }
}
