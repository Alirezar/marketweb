﻿namespace MarketWeb.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_Orders = "Pages.Orders";

        public const string CompaniesARU = "Companies.ARU";

        public const string ProductsARU = "Products.ARU";
    }
}
