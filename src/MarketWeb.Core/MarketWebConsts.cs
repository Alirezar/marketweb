﻿namespace MarketWeb
{
    public class MarketWebConsts
    {
        public const string LocalizationSourceName = "MarketWeb";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = false;
    }
}
