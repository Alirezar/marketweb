﻿using Abp.Domain.Entities;
using MarketWeb.Products;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MarketWeb.Companies
{
    public class Company : Entity<long>
    {
        public const int MaxCompanyNameLength = 60;
        public const int MaxCompanyDiscriptionLength = 500;

        [Required, StringLength(MaxCompanyNameLength)]
        public string Name { get; set; }

        [StringLength(MaxCompanyDiscriptionLength)]
        public string Discription { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
