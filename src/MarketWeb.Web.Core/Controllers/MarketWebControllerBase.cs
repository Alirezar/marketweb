using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace MarketWeb.Controllers
{
    public abstract class MarketWebControllerBase: AbpController
    {
        protected MarketWebControllerBase()
        {
            LocalizationSourceName = MarketWebConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
