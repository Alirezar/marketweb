﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.UI;
using MarketWeb.Carts;
using MarketWeb.Carts.Dtos;
using MarketWeb.Controllers;
using MarketWeb.Orders;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MarketWeb.Web.Controllers
{
    [AbpMvcAuthorize]
    public class CartController : MarketWebControllerBase
    {
        private readonly ICartAppService _cartAppService;
        private readonly IOrderAppService _OrderAppService;
        public CartController(ICartAppService cartAppService, IOrderAppService OrderAppService)
        {
            _cartAppService = cartAppService;
            _OrderAppService = OrderAppService;
        }

        public async Task<IActionResult> Index()
        {
            var cart = await _cartAppService.GetUserCartAsync();
            return View(cart);
        }

        public async Task<IActionResult> Add(long productId)
        {
            if ((await _cartAppService.GetUserCartItemAsync(productId)) != null)
                throw new UserFriendlyException("You have it in cart");
            await _cartAppService.CreateAsync(new CartItemDto { ProductID = productId, UserID = AbpSession.UserId.Value, Count = 1 });
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Remove(long productId)
        {
            await _cartAppService.RemoveItemFromUserCart(productId);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> IncrementItemCount(long productId, int currentCount)
        {
            await _cartAppService.UpdateItemCount(productId, currentCount + 1);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> DecrementItemCount(long productId, int currentCount)
        {
            if (currentCount - 1 != 0)
                await _cartAppService.UpdateItemCount(productId, currentCount - 1);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Confirm()
        {
            var cartItems = await _cartAppService.GetUserCartAsync();
            if (cartItems.Items.Count > 0)
            {
                var order = await _OrderAppService.CreateAsync(cartItems.Items);
                await _cartAppService.CleanUserCart();
                return RedirectToAction(nameof(MyOrdersController.Details), "MyOrders", new { orderId = order.Id });
            }
            return NotFound();
        }
    }
}
