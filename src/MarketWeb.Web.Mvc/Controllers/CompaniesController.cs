﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Authorization;
using MarketWeb.Authorization;
using MarketWeb.Companies;
using MarketWeb.Companies.Dtos;
using MarketWeb.Controllers;
using MarketWeb.Products;
using MarketWeb.Products.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace MarketWeb.Web.Controllers
{
    public class CompaniesController : MarketWebControllerBase
    {
        private readonly ICompanyAppService _companyAppService;
        private readonly IProductAppService _productAppService;
        public CompaniesController(ICompanyAppService companyAppService, IProductAppService productAppService)
        {
            _companyAppService = companyAppService;
            _productAppService = productAppService;
        }

        public async Task<IActionResult> Index(long? id)
        {
            if (id.HasValue)
                return RedirectToAction(nameof(Details), id.Value);

            var companies = await _companyAppService.GetAllAsync(new PagedCompanyResultRequestDto());
            return View(companies.Items);
        }

        public async Task<IActionResult> Details(long id)
        {
            var company = await _companyAppService.GetAsync(new EntityDto<long>(id));
            if (company is null)
                return NotFound();
            return View(company);
        }

        [AbpMvcAuthorize(PermissionNames.CompaniesARU)]
        public IActionResult Create()
        {
            return View();
        }
        [AbpMvcAuthorize(PermissionNames.CompaniesARU)]
        [HttpPost]
        public async Task<IActionResult> Create([Bind("Name,Discription")] CompanyDto company)
        {
            if (ModelState.IsValid)
            {
                var addedCompany = await _companyAppService.CreateAsync(company);
                return RedirectToAction(nameof(Details), new { id = addedCompany.Id });
            }
            return View();
        }

        [AbpMvcAuthorize(PermissionNames.CompaniesARU)]
        public async Task<IActionResult> AddProductToCompany(long companyID)
        {
            ProductDto newProduct = new ProductDto() { CompanyID = companyID, Company = await _companyAppService.GetAsync(new EntityDto<long>(companyID)) };
            return View(newProduct);
        }
        [AbpMvcAuthorize(PermissionNames.CompaniesARU)]
        [HttpPost]
        public async Task<IActionResult> AddProductToCompany([Bind("Name,Price")] ProductDto product, long companyID)
        {
            if (ModelState.IsValid)
            {
                product.CompanyID = companyID;
                await _productAppService.CreateAsync(product);
                return RedirectToAction(nameof(Details), new { id = product.CompanyID });
            }
            return View();
        }
    }
}
