﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using MarketWeb.Authorization;
using MarketWeb.Carts;
using MarketWeb.Controllers;
using MarketWeb.Products;
using MarketWeb.Products.Dtos;
using MarketWeb.Web.Models.Products;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace MarketWeb.Web.Controllers
{
    public class ProductsController : MarketWebControllerBase
    {
        private readonly IProductAppService _productAppService;
        private readonly ICartAppService _cartAppService;
        public ProductsController(IProductAppService productAppService, ICartAppService cartAppService)
        {
            _productAppService = productAppService;
            _cartAppService = cartAppService;
        }

        public async Task<IActionResult> Index(string keyword)
        {
            var products = await _productAppService.GetAllAsync(new PagedProductResultRequestDto { Keyword = keyword });
            var model = new ProductViewModel() { Products = products };
            if (User.Identity.IsAuthenticated)
                model.ProductsInUserCart = await _cartAppService.GetUserCartProductsAsync();
            return View(model);
        }

        public async Task<IActionResult> Details(long id)
        {
            var product = await _productAppService.GetAsync(new EntityDto<long>(id));
            if (product == null)
                return NotFound();
            if (User.Identity.IsAuthenticated)
                ViewBag.IsInCart = (await _cartAppService.GetUserCartProductsAsync()).Select(p => p.Id).Contains(id);
            return View(product);
        }

        [AbpMvcAuthorize(PermissionNames.ProductsARU)]
        public async Task<IActionResult> Delete(long id)
        {
            await _productAppService.DeleteAsync(new EntityDto<long>(id));
            return RedirectToAction(nameof(Index));
        }

        [AbpMvcAuthorize(PermissionNames.ProductsARU)]
        public async Task<IActionResult> Edit(long id)
        {
            var product = await _productAppService.GetAsync(new EntityDto<long>(id));
            if (product is null)
                return NotFound();
            return View(product);
        }
        [AbpMvcAuthorize(PermissionNames.ProductsARU)]
        [HttpPost]
        public async Task<IActionResult> Edit([Bind("Name,Price")] ProductDto product, long id, long companyID)
        {
            if (ModelState.IsValid)
            {
                product.Id = id;
                product.CompanyID = companyID;
                product = await _productAppService.UpdateAsync(product);
                return RedirectToAction(nameof(Details), new { id = product.Id });
            }
            return View();
        }
    }
}
