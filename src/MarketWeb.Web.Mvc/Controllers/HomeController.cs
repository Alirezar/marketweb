﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using MarketWeb.Controllers;

namespace MarketWeb.Web.Controllers
{
    public class HomeController : MarketWebControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
