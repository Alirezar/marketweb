﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using MarketWeb.Controllers;
using MarketWeb.Orders;
using MarketWeb.Orders.Dtos;
using MarketWeb.Products;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketWeb.Web.Controllers
{
    [AbpMvcAuthorize]
    public class MyOrdersController : MarketWebControllerBase
    {
        private readonly IOrderAppService _orderAppService;
        private readonly IProductAppService _productAppService;
        public MyOrdersController(IOrderAppService orderAppService, IProductAppService productAppService)
        {
            _orderAppService = orderAppService;
            _productAppService = productAppService;
        }

        public async Task<IActionResult> Index()
        {
            var orders = await _orderAppService.GetUserOrders();
            foreach (var item in orders.Items)
                await SetOrderItemProducts(item.Items);

            return View(orders.Items);
        }

        public async Task<IActionResult> Details(long orderId)
        {
            var userOrders = await _orderAppService.GetUserOrders();
            var targetOrder = userOrders.Items.FirstOrDefault(o => o.Id == orderId);
            if (targetOrder is null)
                return NotFound();
            await SetOrderItemProducts(targetOrder.Items);
            return View(targetOrder);
        }

        private async Task SetOrderItemProducts(ICollection<OrderItemDto> orderItems)
        {
            foreach (var orderItem in orderItems)
                orderItem.Product = await _productAppService.GetAsync(new EntityDto<long> { Id = orderItem.ProductID });
        }
    }
}
