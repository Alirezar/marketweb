﻿using Abp.Application.Navigation;
using Abp.Authorization;
using Abp.Localization;
using MarketWeb.Authorization;

namespace MarketWeb.Web.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// </summary>
    public class MarketWebNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Home,
                        L("HomePage"),
                        url: "",
                        icon: "fas fa-home",
                        requiresAuthentication: true
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Tenants,
                        L("Tenants"),
                        url: "Tenants",
                        icon: "fas fa-building",
                        permissionDependency: new SimplePermissionDependency(PermissionNames.Pages_Tenants)
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Users,
                        L("Users"),
                        url: "Users",
                        icon: "fas fa-users",
                        permissionDependency: new SimplePermissionDependency(PermissionNames.Pages_Users)
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Roles,
                        L("Roles"),
                        url: "Roles",
                        icon: "fas fa-theater-masks",
                        permissionDependency: new SimplePermissionDependency(PermissionNames.Pages_Roles)
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.About,
                        L("About"),
                        url: "About",
                        icon: "fas fa-info-circle"
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Products,
                        L("Products"),
                        url: "Products",
                        icon: "fas fa-product-hunt"
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Companies,
                        L("Companies"),
                        url: "Companies",
                        icon: "fas fa-building"
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.MyOrders,
                        L("MyOrders"),
                        url: "MyOrders",
                        icon: "fas fa-circle"
                    )
                )
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Cart,
                        L("Cart"),
                        url: "Cart",
                        icon: "fas fa-circle"
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, MarketWebConsts.LocalizationSourceName);
        }
    }
}
