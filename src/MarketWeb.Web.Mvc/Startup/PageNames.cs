﻿namespace MarketWeb.Web.Startup
{
    public class PageNames
    {
        public const string Home = "Home";
        public const string About = "About";
        public const string Tenants = "Tenants";
        public const string Users = "Users";
        public const string Roles = "Roles";
        public const string Products = "Products";
        public const string Companies = "Companies";
        public const string MyOrders = "MyOrders";
        public const string Cart = "Cart";
    }
}
