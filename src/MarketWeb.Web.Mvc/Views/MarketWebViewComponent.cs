﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace MarketWeb.Web.Views
{
    public abstract class MarketWebViewComponent : AbpViewComponent
    {
        protected MarketWebViewComponent()
        {
            LocalizationSourceName = MarketWebConsts.LocalizationSourceName;
        }
    }
}
