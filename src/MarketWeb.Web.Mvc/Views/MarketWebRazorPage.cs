﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace MarketWeb.Web.Views
{
    public abstract class MarketWebRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected MarketWebRazorPage()
        {
            LocalizationSourceName = MarketWebConsts.LocalizationSourceName;
        }
    }
}
