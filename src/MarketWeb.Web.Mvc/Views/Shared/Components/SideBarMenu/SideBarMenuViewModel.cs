﻿using Abp.Application.Navigation;

namespace MarketWeb.Web.Views.Shared.Components.SideBarMenu
{
    public class SideBarMenuViewModel
    {
        public UserMenu MainMenu { get; set; }
    }
}
