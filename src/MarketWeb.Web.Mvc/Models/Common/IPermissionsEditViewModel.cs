﻿using System.Collections.Generic;
using MarketWeb.Roles.Dto;

namespace MarketWeb.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}