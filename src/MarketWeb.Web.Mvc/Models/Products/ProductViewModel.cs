﻿using Abp.Application.Services.Dto;
using MarketWeb.Products.Dtos;
using System.Collections.Generic;

namespace MarketWeb.Web.Models.Products
{
    public class ProductViewModel
    {
        public PagedResultDto<ProductDto> Products { get; set; }
        public ICollection<ProductDto> ProductsInUserCart { get; set; }
    }
}
