﻿using System.Collections.Generic;
using MarketWeb.Roles.Dto;

namespace MarketWeb.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
