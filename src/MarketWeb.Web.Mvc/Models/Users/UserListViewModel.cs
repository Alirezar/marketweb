using System.Collections.Generic;
using MarketWeb.Roles.Dto;

namespace MarketWeb.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
