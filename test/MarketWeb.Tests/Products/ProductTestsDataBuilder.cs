﻿using MarketWeb.Companies;
using MarketWeb.EntityFrameworkCore;

namespace MarketWeb.Tests.Products
{
    public class ProductTestsDataBuilder
    {
        public Company Company { get; private set; }

        public void Build(MarketWebDbContext context, int buildProductCount)
        {
            Company = DataBuilder.BuildCompany(context);
            for (int i = 0; i < buildProductCount; i++)
                DataBuilder.BuildProduct(context, Company.Id, "product " + i.ToString());
        }
    }
}
