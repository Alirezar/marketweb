﻿using MarketWeb.Products;
using MarketWeb.Products.Dtos;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace MarketWeb.Tests.Products
{
    public class ProductAppService_Test : MarketWebTestBase
    {
        private const int buildProductCount = 3;

        private readonly IProductAppService _productAppService;
        private readonly ProductTestsDataBuilder dataBuilder;
        public ProductAppService_Test()
        {
            _productAppService = Resolve<IProductAppService>();
            dataBuilder = new ProductTestsDataBuilder();
            UsingDbContext(context => dataBuilder.Build(context, buildProductCount));
        }

        [Fact]
        public async Task Create_Product_Test()
        {
            var addedProduct = await _productAppService.CreateAsync(
                new ProductDto()
                {
                    Name = "product1",
                    Price = 123.5M,
                    CompanyID = dataBuilder.Company.Id
                });

            await UsingDbContext(async context =>
            {
                var product1 = await context.Products.FirstOrDefaultAsync(p => p.Id == addedProduct.Id);
                product1.ShouldNotBeNull();
            });
        }

        [Fact]
        public async Task Get_All_Products_Test()
        {
            var products = await _productAppService.GetAllAsync(new PagedProductResultRequestDto());
            products.TotalCount.ShouldBe(buildProductCount);
        }

        [Fact]
        public async Task Products_Company_Property_Test()
        {
            var products = await _productAppService.GetAllAsync(new PagedProductResultRequestDto());
            foreach (var item in products.Items)
                item.Company.ShouldNotBeNull();
        }

    }
}
