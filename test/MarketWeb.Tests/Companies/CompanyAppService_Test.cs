﻿using Abp.Application.Services.Dto;
using MarketWeb.Companies;
using MarketWeb.Companies.Dtos;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace MarketWeb.Tests.Companies
{
    public class CompanyAppService_Test : MarketWebTestBase
    {
        private const int companyCount = 3;
        private const int eachCompanyProductCount = 3;

        private readonly ICompanyAppService _companyAppService;
        private readonly CompaniesTestsDataBuilder dataBuilder;
        public CompanyAppService_Test()
        {
            _companyAppService = Resolve<ICompanyAppService>();
            dataBuilder = new CompaniesTestsDataBuilder();
            UsingDbContext(context => dataBuilder.Build(context, companyCount, eachCompanyProductCount));
        }

        [Fact]
        public async Task Get_All_Company_Test()
        {
            var companies = await _companyAppService.GetAllAsync(new PagedCompanyResultRequestDto());
            companies.TotalCount.ShouldBe(companyCount);
        }

        [Fact]
        public async Task Companis_Products_Test()
        {
            var company = await _companyAppService.GetAsync(new EntityDto<long> { Id = dataBuilder.Companies[0].Id });
            company.Products.Count.ShouldBe(eachCompanyProductCount);
        }
    }
}
