﻿using MarketWeb.Companies;
using MarketWeb.EntityFrameworkCore;
using MarketWeb.Products;

namespace MarketWeb.Tests.Companies
{
    public class CompaniesTestsDataBuilder
    {
        public Company[] Companies { get; private set; }
        public Product[] Products { get; private set; }

        public void Build(MarketWebDbContext context, int companyCount, int eachCompanyProductCount)
        {
            Companies = new Company[companyCount];
            Products = new Product[companyCount * eachCompanyProductCount];

            for (int i = 0; i < companyCount; i++)
            {
                Companies[i] = DataBuilder.BuildCompany(context, "Company" + i.ToString());
                for (int j = 0; j < eachCompanyProductCount; j++)
                {
                    Products[j]= DataBuilder.BuildProduct(context, Companies[i].Id, Companies[i].Name + " Product " + j.ToString());
                }
            }
        }
    }
}
