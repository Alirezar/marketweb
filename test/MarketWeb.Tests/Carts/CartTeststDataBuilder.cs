﻿using MarketWeb.Carts;
using MarketWeb.Companies;
using MarketWeb.EntityFrameworkCore;
using MarketWeb.Products;

namespace MarketWeb.Tests.Carts
{
    public class CartTeststDataBuilder
    {
        private Company company;
        private Product[] products;

        public CartItem[] CartItems { get; private set; }

        public void Build(MarketWebDbContext context, long userId, int cartItemsCount)
        {
            company = DataBuilder.BuildCompany(context);
            products = new Product[cartItemsCount];
            for (int i = 0; i < cartItemsCount; i++)
                products[i] = DataBuilder.BuildProduct(context, company.Id, "Product" + i.ToString(), i * 100);

            CartItems = new CartItem[cartItemsCount];
            for (int i = 0; i < cartItemsCount; i++)
                CartItems[i] = DataBuilder.BuildCartItem(context, userId, products[i].Id);
        }
    }
}
