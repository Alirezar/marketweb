﻿using Abp.Application.Services.Dto;
using MarketWeb.Carts;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace MarketWeb.Tests.Carts
{
    public class CartAppService_Test : MarketWebTestBase
    {
        private const int CartItemsCount = 3;

        private readonly ICartAppService _cartAppService;
        private readonly CartTeststDataBuilder dataBuilder;
        public CartAppService_Test()
        {
            _cartAppService = Resolve<ICartAppService>();
            dataBuilder = new CartTeststDataBuilder();
            UsingDbContext(context => dataBuilder.Build(context, AbpSession.UserId.Value, CartItemsCount));
        }

        [Fact]
        public async Task GetUserCart_Test()
        {
            var cart = await _cartAppService.GetUserCartAsync();
            cart.ShouldNotBeNull();
            cart.Items.Count.ShouldBe(CartItemsCount);
        }

        [Fact]
        public async Task Test_TotalItemPrice()
        {
            var cart = await _cartAppService.GetAsync(new EntityDto<long> { Id = dataBuilder.CartItems[0].Id });
            cart.ShouldNotBeNull();
            cart.TotalPrice().ShouldBe(cart.Product.Price * cart.Count);
        }
    }
}
