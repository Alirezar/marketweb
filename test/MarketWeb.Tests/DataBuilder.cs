﻿using MarketWeb.Carts;
using MarketWeb.Companies;
using MarketWeb.EntityFrameworkCore;
using MarketWeb.Products;

namespace MarketWeb.Tests
{
    public static class DataBuilder
    {
        public static Company BuildCompany(MarketWebDbContext context, string companyName = "TestCompany")
        {
            return context.Companies.Add(new Company { Name = companyName }).Entity;
        }

        public static Product BuildProduct(MarketWebDbContext context, long companyId, string name = "product", decimal price = 0M)
        {
            return context.Products.Add(new Product { Name = name, Price = price, CompanyID = companyId }).Entity;
        }

        public static CartItem BuildCartItem(MarketWebDbContext context, long userId, long productId, int count = 1)
        {
            return context.CartItems.Add(new CartItem { UserID = userId, ProductID = productId, Count = count }).Entity;
        }
    }
}
