﻿using System.Threading.Tasks;
using MarketWeb.Models.TokenAuth;
using MarketWeb.Web.Controllers;
using Shouldly;
using Xunit;

namespace MarketWeb.Web.Tests.Controllers
{
    public class HomeController_Tests: MarketWebWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}