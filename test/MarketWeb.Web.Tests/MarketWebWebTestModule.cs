﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using MarketWeb.EntityFrameworkCore;
using MarketWeb.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace MarketWeb.Web.Tests
{
    [DependsOn(
        typeof(MarketWebWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class MarketWebWebTestModule : AbpModule
    {
        public MarketWebWebTestModule(MarketWebEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MarketWebWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(MarketWebWebMvcModule).Assembly);
        }
    }
}